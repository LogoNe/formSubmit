create table contact_form
(
    id                 int8 not null,
    created_at         timestamp,
    updated_at         timestamp,
    customer_request   varchar(255),
    first_name         varchar(255),
    last_name          varchar(255),
    policy_number      varchar(255),
    kind_of_request_id int8,
    primary key (id)
);
create table kind_of_request
(
    id           int8 not null,
    created_at   timestamp,
    updated_at   timestamp,
    request_kind varchar(255),
    primary key (id)
);
create sequence hibernate_sequence start 1 increment 1;
alter table if exists contact_form
    add constraint FK_contact_form_kind_of_request foreign key (kind_of_request_id) references kind_of_request