package com.example.lundegaard.service.impl;

import com.example.lundegaard.domain.ContactForm;
import com.example.lundegaard.mapper.ContactFormMapper;
import com.example.lundegaard.model.ContactFormDto;
import com.example.lundegaard.repository.ContactFormRepository;
import com.example.lundegaard.service.ContactFormService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class ContactFormServiceImpl implements ContactFormService {
    private final ContactFormRepository contactFormRepository;
    private final ContactFormMapper contactFormMapper;

    @Override
    @Transactional
    public ContactFormDto saveContact(ContactFormDto contactRequest) {
        ContactForm contactForm = contactFormMapper.mapToDomain(contactRequest);
        return contactFormMapper.mapToDto(contactFormRepository.save(contactForm));
    }
}
