package com.example.lundegaard.service.impl;

import com.example.lundegaard.domain.KindOfRequest;
import com.example.lundegaard.mapper.KindOfRequestMapper;
import com.example.lundegaard.model.KindOfRequestDto;
import com.example.lundegaard.repository.KindOfRequestRepository;
import com.example.lundegaard.service.KindOfRequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class KindOfRequestServiceImpl implements KindOfRequestService {
    private final KindOfRequestRepository kindOfRequestRepository;
    private final KindOfRequestMapper kindOfRequestMapper;

    @Override
    public List<KindOfRequestDto> loadRequests() {
      return kindOfRequestRepository.findAll().stream()
              .map(kindOfRequestMapper::mapToDto)
              .collect(Collectors.toList());
    }

    @Override
    public KindOfRequest findRequest(Long kindOfRequestId) {
        return kindOfRequestRepository.findById(kindOfRequestId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid request id"));
    }
}
