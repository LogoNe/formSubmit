package com.example.lundegaard.service;

import com.example.lundegaard.domain.KindOfRequest;
import com.example.lundegaard.model.KindOfRequestDto;

import java.util.List;

public interface KindOfRequestService {
    /**
     * Loads requests from db to a list
     * @return a list of request options
     */
    List<KindOfRequestDto> loadRequests();

    /**
     * Finds kind of request based on recieved id
     * @param kindOfRequestId id of the request
     * @return kind of request object
     */
    KindOfRequest findRequest(Long kindOfRequestId);

}
