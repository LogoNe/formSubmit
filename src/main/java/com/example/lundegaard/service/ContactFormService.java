package com.example.lundegaard.service;

import com.example.lundegaard.model.ContactFormDto;

public interface ContactFormService {
    /**
     * Maps contact to ContactForm and saves contact
     * @param contact contact information a customer has entered
     * @return object of ContactFormDto
     */
    ContactFormDto saveContact(ContactFormDto contact);
}
