package com.example.lundegaard.domain;

import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class KindOfRequest extends AbstractEntity{
    private String requestKind;
}
