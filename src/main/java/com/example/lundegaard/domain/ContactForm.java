package com.example.lundegaard.domain;

import com.example.lundegaard.validator.NameValidation;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.validation.constraints.Digits;

@Data
@Entity
public class ContactForm extends AbstractEntity{
    @OneToOne
    private KindOfRequest kindOfRequest;
    @Digits(integer = 20, fraction =0 )
    private String policyNumber;
    @NameValidation
    private String firstName;
    @NameValidation
    private String lastName;
    @Length(max = 255)
    private String CustomerRequest;
}
