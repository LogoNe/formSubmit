package com.example.lundegaard.mapper;

import com.example.lundegaard.domain.KindOfRequest;
import com.example.lundegaard.model.KindOfRequestDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface KindOfRequestMapper {
    KindOfRequestDto mapToDto(KindOfRequest source);
}
