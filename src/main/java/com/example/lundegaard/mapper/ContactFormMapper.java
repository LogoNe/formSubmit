package com.example.lundegaard.mapper;

import com.example.lundegaard.domain.ContactForm;
import com.example.lundegaard.model.ContactFormDto;
import com.example.lundegaard.service.KindOfRequestService;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = "spring")
public abstract class ContactFormMapper {
    @Autowired
    private KindOfRequestService kindOfRequestService;
    @Mapping(target = "kindOfRequest", ignore = true)
    public abstract ContactFormDto mapToDto(ContactForm source);
    @Mapping(target = "kindOfRequest", ignore = true)
    public abstract ContactForm mapToDomain(ContactFormDto source);
    @AfterMapping
    public void after(@MappingTarget ContactForm target, ContactFormDto source) {
        target.setKindOfRequest(kindOfRequestService.findRequest(source.getKindOfRequest()));
    }
}