package com.example.lundegaard.api;

import com.example.lundegaard.model.ContactFormDto;
import com.example.lundegaard.model.KindOfRequestDto;
import com.example.lundegaard.service.ContactFormService;
import com.example.lundegaard.service.KindOfRequestService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@RequestMapping("/contactUs")
@RequiredArgsConstructor
@Controller
public class ContactFormController {
    private final ContactFormService contactFormService;
    private final KindOfRequestService kindOfRequestService;

    @GetMapping
    public String contactUsForm(Model model) {
        model.addAttribute("contactFormDto", new ContactFormDto());
        List<KindOfRequestDto> listOfRequests = kindOfRequestService.loadRequests();
        model.addAttribute("requestKinds", listOfRequests);
        return "contactForm";
    }

    @PostMapping
    public String contactUsFormSubmit(@ModelAttribute ContactFormDto contactFormDto, Model model) {
        model.addAttribute("contactFormDto", contactFormDto);
        contactFormService.saveContact(contactFormDto);
        return "result";
    }

}
