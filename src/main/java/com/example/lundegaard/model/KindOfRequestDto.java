package com.example.lundegaard.model;

import lombok.Data;

@Data
public class KindOfRequestDto {
    private Long id;
    private String requestKind;
}
