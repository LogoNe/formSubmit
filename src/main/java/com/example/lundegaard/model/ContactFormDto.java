package com.example.lundegaard.model;

import lombok.Data;

@Data
public class ContactFormDto {
    private Long kindOfRequest;
    private String policyNumber;
    private String firstName;
    private String lastName;
    private String CustomerRequest;
}
