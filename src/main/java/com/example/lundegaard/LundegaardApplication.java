package com.example.lundegaard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LundegaardApplication {

	public static void main(String[] args) {
		SpringApplication.run(LundegaardApplication.class, args);
	}

}
