package com.example.lundegaard.repository;

import com.example.lundegaard.domain.KindOfRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KindOfRequestRepository extends CrudRepository<KindOfRequest, Long> {
    List<KindOfRequest> findAll();
}
