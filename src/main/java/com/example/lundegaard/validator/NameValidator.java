package com.example.lundegaard.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NameValidator implements ConstraintValidator<NameValidation, String> {
    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {

        return s.matches("^[a-zA-Z]+$");

    }

    @Override
    public void initialize(NameValidation constraintAnnotation) {

    }
}